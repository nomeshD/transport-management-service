package org.sltb.transportmanagement.controller;

import org.sltb.transportmanagement.core.CreateJourneyRequest;
import org.sltb.transportmanagement.core.WebResponse;
import org.sltb.transportmanagement.domain.Journey;
import org.sltb.transportmanagement.exception.InsufficientBalanceException;
import org.sltb.transportmanagement.service.JourneyManagementService;
import org.sltb.transportmanagement.service.PassengerManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Nomesh De Silva
 *
 */
@Controller
public class TransportManagementController {

	@Autowired
	private PassengerManagementService memberShipManagementService;
	@Autowired
	private JourneyManagementService journayManagementService;

	@RequestMapping(method = {
			RequestMethod.GET }, path = "/passengers/{userId}/cantravel", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public WebResponse canTakeJouney(@PathVariable("userId") long userId) {
		boolean canTakJourney;
		try {
			canTakJourney = memberShipManagementService.canTakeJouney(userId);

		} catch (InsufficientBalanceException e) {
			return new WebResponse.WebResponseBuilder<Boolean>().addResponseData(false)
					.addResponseStatus(HttpStatus.BAD_REQUEST.value()).addMessage(e.getMessage()).build();
		} catch (Exception e) {
			return new WebResponse.WebResponseBuilder<Boolean>().addResponseData(false)
					.addResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR.value()).addMessage(e.getMessage()).build();
		}
		return new WebResponse.WebResponseBuilder<Boolean>().addResponseData(canTakJourney)
				.addResponseStatus(HttpStatus.OK.value()).addMessage("").build();
	}

	@RequestMapping(method = {
			RequestMethod.POST }, path = "/journeys", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public WebResponse createJourney(@RequestBody CreateJourneyRequest journeyRequest) {
		Journey createdJourney;
		try {
			createdJourney = journayManagementService.create(journeyRequest.getStartingZone(),
					journeyRequest.getEndingZone(), journeyRequest.getUserId(), journeyRequest.getStartTime());

		} catch (Exception e) {
			return new WebResponse.WebResponseBuilder<Journey>().addResponseData(null)
					.addResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR.value()).addMessage(e.getMessage()).build();
		}
		return new WebResponse.WebResponseBuilder<Journey>().addResponseData(createdJourney)
				.addResponseStatus(HttpStatus.OK.value()).addMessage("").build();
	}

	@RequestMapping(method = {
			RequestMethod.PUT }, path = "/journeys/{journeyId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public WebResponse endJourney(@PathVariable("journeyId") final Long journeyId,
			@RequestParam("userId") final Long userId, @RequestParam("startZone") final String startZone,
			@RequestParam("endZone") final String endZone) {
		Double remainingBalance;
		try {
			remainingBalance = journayManagementService.end(journeyId, userId, startZone, endZone);

		} catch (Exception e) {
			return new WebResponse.WebResponseBuilder<Double>().addResponseData(null)
					.addResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR.value()).addMessage(e.getMessage()).build();
		}
		return new WebResponse.WebResponseBuilder<Double>().addResponseData(remainingBalance)
				.addResponseStatus(HttpStatus.OK.value()).addMessage("").build();
	}

	@RequestMapping(method = {
			RequestMethod.POST }, path = "/passengers/cards/{userId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public WebResponse requestCard(@PathVariable("userId") final Long userId) {
		Boolean cardRequestCompleted;
		try {
			cardRequestCompleted = memberShipManagementService.requestCard(userId);

		} catch (Exception e) {
			return new WebResponse.WebResponseBuilder<Boolean>().addResponseData(null)
					.addResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR.value()).addMessage(e.getMessage()).build();
		}
		return new WebResponse.WebResponseBuilder<Boolean>().addResponseData(cardRequestCompleted)
				.addResponseStatus(HttpStatus.OK.value()).addMessage("").build();
	}

	@RequestMapping(method = {
			RequestMethod.GET }, path = "/journeys/{userId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public WebResponse historyOf(@PathVariable("userId") final Long userId) {
		List<Journey> journeys;
		try {
			journeys = journayManagementService.historyOf(userId);

		} catch (Exception e) {
			return new WebResponse.WebResponseBuilder<List<Journey>>().addResponseData(null)
					.addResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR.value()).addMessage(e.getMessage()).build();
		}
		return new WebResponse.WebResponseBuilder<List<Journey>>().addResponseData(journeys)
				.addResponseStatus(HttpStatus.OK.value()).addMessage("").build();
	}

}
